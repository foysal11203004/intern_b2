
<link rel="stylesheet" href="{{url('/css/app.css')}}">
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6" style="padding-top: 10px;">

        <form action="{{route('create.product.post')}}" method="POST" role="form">
            @csrf
            <div class="form-group">
                <label for="">Product Name:</label>
                <input name="product_name" type="text" class="form-control" id="" placeholder="Enter Product Name.." required>
            </div>
            <div class="form-group">
                <label for="">Product Price:</label>
                <input name="product_price" type="number" class="form-control" id="" placeholder="Enter Product price.." required>
            </div>

            <div class="form-group">
                <label for="">Description:</label>
                <textarea name="product_description" class="form-control" id="" rows="3" placeholder="Enter Product Description"></textarea>
            </div>

            <button type="submit" class="btn btn-primary">Submit</button>
            <button class="btn btn-danger">Cancel</button>
        </form>



    </div>
</div>
