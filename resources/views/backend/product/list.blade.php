

@extends('master')
@section('content')

    @if(session()->has('message'))
        <div class="alert alert-success">
            {{ session()->get('message') }}
        </div>
    @endif

    <!-- Button trigger modal -->
    <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#exampleModal">
        Add Product
    </button>
    <p style="padding-top: 20px;"></p>

    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Serial</th>
            <th>Category Name</th>
            <th>Product Name</th>
            <th>Price</th>
        </tr>
        </thead>
        <tbody>


        @foreach($all_products as  $key=>$data)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$data->category_id}}</td>
            <td>{{$data->name}}</td>
            <td>{{$data->price}}</td>
        </tr>
        @endforeach
        {{$all_products->links()}}
        </tbody>
    </table>





    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>


                <form action="{{route('product.create')}}" method="POST" role="form">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="productName">Product Name:</label>
                            <input class="form-control" id="productName" type="text" name="product_name" placeholder="Enter Product Name...">
                        </div>

                        <div class="form-group">
                            <label for="price">Product Price:</label>
                            <input class="form-control" id="price" type="text" name="price" placeholder="Enter Price...">
                        </div>
                        <div class="form-group">
                            <label for="category">Select Category:</label>
                            <select class="form-control" name="category_id" id="category">
                        @foreach($categories as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                            </select>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save changes</button>
                    </div>
                </form>


            </div>
        </div>
    </div>
@stop

