<?php

namespace App\Http\Controllers\Frontend;

use App\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProductController extends Controller
{

    public function home()
    {
        return view('backend.dashboard');

    }

    public function order()
    {
        return view('backend.order');

    }





    public function productList()
    {
        $all_products=Product::get();
        return view('Frontend.products_list',compact('all_products'));
    }

    public function productCreate()
    {

        return view('Frontend.product_create');
    }

    public function productCreatePost(Request $request)
    {
        $data=[
            'name'=>$request->input('product_name'),
            'price'=>$request->input('product_price'),
            'description'=>$request->input('product_description')
        ];

        Product::create($data);
        return redirect()->back();
    }

}
