<?php

namespace App\Http\Controllers\Backend;

use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
class ProductController extends Controller
{
    public function productList()
    {
        $categories=Category::all();//select * from categories
        $all_products=Product::paginate(5);

        return view('backend.product.list',compact('categories','all_products'));
    }

    public function create(Request $request)
    {

      Product::create([
          'name'=>$request->input('product_name'),
          'price'=>$request->input('price'),
          'category_id'=>$request->input('category_id'),
      ]);

        session()->flash('message', 'Product Added Successfully!');
        return redirect()->back();

    }
}
