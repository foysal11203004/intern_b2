<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','Frontend\ProductController@home')->name('backend.dashboard');
Route::get('/order','Frontend\ProductController@order')->name('order');




Route::get('/product','Backend\ProductController@productList')->name('product.list');
Route::post('/product/create','Backend\ProductController@create')->name('product.create');




//Route::get('/','Frontend\ProductController@productList');
Route::get('/add_new_product','Frontend\ProductController@productCreate')->name('create.product');
Route::post('/add_new_product/post','Frontend\ProductController@productCreatePost')->name('create.product.post');
